package com.cybertech.toolbardiplo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.cybertech.recyclerdiplo.models.Book
import com.cybertech.toolbardiplo.MainActivity.Companion.RESULT_ERROR_REGISTER_BOOK
import com.cybertech.toolbardiplo.MainActivity.Companion.RESULT_REGISTER_BOOK
import kotlinx.android.synthetic.main.activity_register_book.*
import kotlinx.android.synthetic.main.item_book.*

class RegisterBookActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_book)
        setSupportActionBar(registerBookToolbar)

        supportActionBar?.apply {
            title="Registro de libro"
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        saveBookButton.setOnClickListener {
            val title = titleTextInputLayout.editText?.text.toString()
            val publisher = publisherTextInputLayout.editText?.text.toString()
            val author = authorTextInputLayout.editText?.text.toString()
            val pages = pagesTextInputLayout.editText?.text.toString()
            if(title.isNotEmpty() && pages.isNotEmpty()) {
                val id = title.length * 5
                val newBook = Book(id, title, publisher, pages.toInt(), author)
                val bookIntent = Intent(this, MainActivity::class.java).apply {
                    val bookBundle = Bundle().apply {
                        putParcelable("book", newBook)
                    }
                    putExtras(bookBundle)
                }
                setResult(RESULT_REGISTER_BOOK,bookIntent)
            }else{
                setResult(RESULT_ERROR_REGISTER_BOOK)
            }
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}