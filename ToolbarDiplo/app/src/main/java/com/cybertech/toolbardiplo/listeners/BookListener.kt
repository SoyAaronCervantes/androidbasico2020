package com.cybertech.recyclerdiplo.listeners

import com.cybertech.recyclerdiplo.models.Book

interface BookListener {

    fun onClickBook(book: Book)

    fun onShareBook(book: Book)
}