package com.cybertech.recyclerdiplo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.recyclerdiplo.adapters.viewholders.BookViewHolder
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book
import com.cybertech.toolbardiplo.R


class BookAdapter(val books: ArrayList<Book>) : RecyclerView.Adapter<BookViewHolder>() {

    private var bookListener: BookListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        val bookViewHolder = BookViewHolder(itemView)
        bookViewHolder.setBookListener(bookListener)
        return bookViewHolder
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bindBook(books[position])
    }

    override fun getItemCount(): Int {
        return books.size
    }

    fun updateBook(book: Book) {
        if (books.isNotEmpty()) {
            books.add(book)
            notifyDataSetChanged()
        }
    }

    fun cleanBooks() {
        books.clear()
        notifyDataSetChanged()
    }

    fun updateBook(book: Book, position: Int) {
        if (books.isNotEmpty() && position < books.size) {
            books.add(position, book)
            notifyItemInserted(position)
        }
    }

    fun setBookListener(listener: BookListener?) {
        this.bookListener = listener
    }
}