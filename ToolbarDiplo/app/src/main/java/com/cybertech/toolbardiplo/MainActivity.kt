package com.cybertech.toolbardiplo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.recyclerdiplo.adapters.BookAdapter
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), BookListener {

    companion object{
        const val RESULT_REGISTER_BOOK=525
        const val RESULT_ERROR_REGISTER_BOOK=526
    }

    private var bookAdapter:BookAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val books = arrayListOf<Book>(
            Book(1,
                "Codigo Da Vinci",
                "Editorial",
                468,
                "Dan Brown"),
            Book(2,
                "Angeles y Demonios",
                "Editorial",
                234,
                "Dan Brown"),
            Book(3,
                "Inferno",
                "Editorial",
                445,
                "Dan Brown"),
            Book(4,
                "El Señor de los Anillos",
                "Editorial",
                134,
                "Philps Coulsen"),
            Book(5,
                "El Señor de los Anillos: Retorno del Rey",
                "Editorial",
                4567,
                "Philps Coulsen")
        )
        bookAdapter = BookAdapter(books)
        bookAdapter?.setBookListener(this)
        bookAdapter?.let {
            booksRecyclerView.adapter=it
        }
        val layoutManager = LinearLayoutManager(baseContext,
            RecyclerView.VERTICAL, false)
        booksRecyclerView.layoutManager = layoutManager
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.registerBookItem -> {
                val registerIntent= Intent(this,RegisterBookActivity::class.java)
                startActivityForResult(registerIntent, RESULT_REGISTER_BOOK)
                return true
            }
            R.id.addBooItem -> {
                val book = Book(1212,"EL rio que fluye","McGrawHill",321,"Paulo Cohelo")
                bookAdapter?.updateBook(book,1)
                return true
            }
            R.id.cleanBooksItem -> {
                bookAdapter?.cleanBooks()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode== RESULT_REGISTER_BOOK && resultCode== RESULT_REGISTER_BOOK){
            var book:Book? = null
            data?.let {
                book=it.getParcelableExtra("book")
                book?.let { it1 ->
                    bookAdapter?.updateBook(it1)
                }
            }

        }else if(requestCode== RESULT_REGISTER_BOOK && resultCode == RESULT_ERROR_REGISTER_BOOK){
            Toast.makeText(this,"Algo salio mal",Toast.LENGTH_LONG).show()
        }
    }

    override fun onClickBook(book: Book) {
        TODO("Not yet implemented")
    }

    override fun onShareBook(book: Book) {
        TODO("Not yet implemented")
    }
}