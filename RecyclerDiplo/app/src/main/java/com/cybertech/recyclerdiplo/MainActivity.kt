package com.cybertech.recyclerdiplo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.cybertech.recyclerdiplo.adapters.BookAdapter
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BookListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val books = arrayListOf<Book>(
            Book(1,
                "Codigo Da Vinci",
                "Editorial",
                468,
                "Dan Brown"),
            Book(2,
                "Angeles y Demonios",
                "Editorial",
                234,
                "Dan Brown"),
            Book(3,
                "Inferno",
                "Editorial",
                445,
                "Dan Brown"),
            Book(4,
                "El Señor de los Anillos",
                "Editorial",
                134,
                "Philps Coulsen"),
            Book(5,
                "El Señor de los Anillos: Retorno del Rey",
                "Editorial",
                4567,
                "Philps Coulsen")
        )
        val bookAdapter = BookAdapter(books)
        bookAdapter.setBookListener(this)
        booksRecyclerView.adapter=bookAdapter
        val layoutManager = StaggeredGridLayoutManager(3,RecyclerView.VERTICAL)
        booksRecyclerView.layoutManager=layoutManager
    }

    override fun onClickBook(book: Book) {
        Toast.makeText(this,"Le dio click a la vista",Toast.LENGTH_LONG).show()
    }

    override fun onShareBook(book: Book) {
        Toast.makeText(this,"Le dio click al button",Toast.LENGTH_LONG).show()
    }
}