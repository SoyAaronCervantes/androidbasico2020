package com.cybertech.fragmentsdiplo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cybertech.fragmentsdiplo.fragments.MainFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val isTablet = resources.getBoolean(R.bool.isTablet)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer,MainFragment.newInstance())
            .commit()

    }
}