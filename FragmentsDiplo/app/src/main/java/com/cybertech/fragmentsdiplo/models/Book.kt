package com.cybertech.recyclerdiplo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    val id: Int = 0,
    val title: String = "",
    val publisher: String = "",
    val pages: Int = 0,
    val author: String
) : Parcelable
