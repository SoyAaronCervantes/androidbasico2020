package com.cybertech.fragmentsdiplo.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cybertech.fragmentsdiplo.DetailActivity
import com.cybertech.fragmentsdiplo.R
import com.cybertech.recyclerdiplo.adapters.BookAdapter
import com.cybertech.recyclerdiplo.listeners.BookListener
import com.cybertech.recyclerdiplo.models.Book
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment(), BookListener {

    private var bookAdapter: BookAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val books = arrayListOf<Book>(
            Book(
                1,
                "Codigo Da Vinci",
                "Editorial",
                468,
                "Dan Brown"
            ),
            Book(
                2,
                "Angeles y Demonios",
                "Editorial",
                234,
                "Dan Brown"
            ),
            Book(
                3,
                "Inferno",
                "Editorial",
                445,
                "Dan Brown"
            ),
            Book(
                4,
                "El Señor de los Anillos",
                "Editorial",
                134,
                "Philps Coulsen"
            ),
            Book(
                5,
                "El Señor de los Anillos: Retorno del Rey",
                "Editorial",
                4567,
                "Philps Coulsen"
            )
        )
        bookAdapter = BookAdapter(books)
        bookAdapter?.setBookListener(this)
        bookAdapter?.let {
            booksRecyclerView.adapter = it
        }
        val layoutManager = LinearLayoutManager(
            requireContext(),
            RecyclerView.VERTICAL, false
        )
        booksRecyclerView.layoutManager = layoutManager
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            MainFragment()

    }

    override fun onClickBook(book: Book) {
        val isTablet= resources.getBoolean(R.bool.isTablet)
        if(isTablet){
            requireFragmentManager()
                .beginTransaction()
                .replace(R.id.detailContainer,DetailBookFragment.newInstance(book))
                .commit()
        }else{
            val detailIntent = Intent(requireContext(),DetailActivity::class.java).apply {
                val detailBundle = Bundle().apply {
                    putParcelable("book",book)
                }
                putExtras(detailBundle)
            }
            startActivity(detailIntent)
        }
    }

    override fun onShareBook(book: Book) {
        TODO("Not yet implemented")
    }
}