package com.cybertech.testdiplo.models

import android.os.Parcel
import android.os.Parcelable

data class Teacher(var id: Int = 0, var name: String ?= ""): Parcelable{
    constructor(parcel: Parcel) : this(
        id=parcel.readInt(),
        name=parcel.readString()

    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Teacher> {
        override fun createFromParcel(parcel: Parcel): Teacher {
            return Teacher(parcel)
        }

        override fun newArray(size: Int): Array<Teacher?> {
            return arrayOfNulls(size)
        }
    }

}
