package com.cybertech.testdiplo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Director(var id: Int = 0, var name: String = ""): Parcelable
